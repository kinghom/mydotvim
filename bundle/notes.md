# Plugins Notes

## Autocomplete and Snippets:
* Snipmate/UltiSnips + SuperTab + AutoComplPop
* neocomplcache/neosnippet: autocomplete/snippet support as a fallback choice when YCM and/or python is unavailable
* YouCompleteMe(compiled YCM), neocomplete(lua)
* vim-snippets: snippets collection
* vim-cycle

## Alignment:
* tabular: easily aligns code
* easy-align: easily aligns code
* align
* alignta
* vim-lion: use `gl` and `gL` to insert space

## statusline:
* lightline
* airline
* bufferline: simple plugin which prints all your open buffers in the command bar

## mark:
* ShowMarks
* mark

## Plugin Management:
* Pathogen
* Vundle
* NeoBundle

## File Searching:
* CommandT(In Ruby)
* CtrlP: fuzzy file searching
* FuzzyFinder
* unite: search and display information from arbitrary sources, like files, buffers, recently used files or registers
* lookupfile: opening files by typing a pattern

## parenthes:
* auto-pairs
* delimitmate: automagically adds closing quotes and braces
* rainbow_parentheses: colorful parentheses

## tag:
* taglist
* tagbar

## comment:
* EnhCommentify
* nerdcommenter
* tcomment
* commentary: nice comment

## buffer:
* minibufexpl
* eunuch: Delete or rename a buffer

## motion

easily jumps to any character on the screen

* CamelCaseMotion
* easymotion
* sneak
* seek
* vim-vertical-move: `,[`, `,]` for visual up and down select

## Search and Replace:
* multiple-cursors:  select all matching words and lets you concurrently change all matches at the same time
* over: substitute preview

## git:
* fugitive: git wrapper
* gitv: nice log history viewer for git
* vcscommand

## File Browser:
* nerdtree
* nerdtree-tabs
* SrcExpl
* vimfiler

## color:
 badwolf cs-gruvbox cs-kolor cs-peaksea zenburn vividchalk vim-hybrid vim-vombato molokai desertink inkpot irblack skittles_berry

## undo:
* gundo: Graph your undo tree
* yankring

## Visual-block:
* VisIncr
* niceblock
* vis

## wiki:
* vimwiki

## syntax:
* markdown
* syntastic: awesome syntax checking for a variety of languages
* vim-perl
* uvm_gen
* verilog_systemverilog
* vim-orgmode
* c

## Draw:
* DrawIt
* table-mode

## library:
* L9
* tlib_vim

unimpaired: many additional bracket [] maps
surround: makes for quick work of surrounds
repeat: repeat plugin commands
speeddating: Ctrl+A and Ctrl+X for dates
matchit: makes your % more awesome
signify: adds + and - to the signs column when changes are detected to source control files (supports git/hg/svn)
startify: gives you a better start screen
ColorV: is a color view/pick/edit/design/scheme tool
ScrollColor, csExplorer: explorer colorschemes
Conque-Shell: is a Vim plugin which allows you to run interactive programs, such as bash on linux or powershell.exe on Windows, inside a Vim buffer
file-line: open the file under cursor, and goto the line specified after the filename
headlights: add a menu to vim, revealing all plugins used
indent-guides: visually displaying indent levels in Vim

AnsiEsc
FencView
calendar
cscope_macros
utl
vim-addon-mw-utils
